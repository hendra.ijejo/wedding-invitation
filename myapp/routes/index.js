var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/:name', function(req, res, next) {
  const { name } = req.params
  const nameUser = name? name: 'you'
  res.render('index', { title: 'Express', name: nameUser });
});

module.exports = router;
